package fr.uavignon.ceri.tp3.data.webservice;

public class Weather {

    public final String main;
    public final String description;
    public final String icon;

    public Weather(String main, String description, String icon) {
        this.main = main;
        this.description = description;
        this.icon = icon;
    }
}
