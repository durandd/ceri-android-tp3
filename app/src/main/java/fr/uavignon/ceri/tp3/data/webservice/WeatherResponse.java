package fr.uavignon.ceri.tp3.data.webservice;

import java.util.List;

public class WeatherResponse {
    public final List<Weather> weather;
    public final MainInfos main;
    public final Wind wind;
    public final Cloud clouds;

    public WeatherResponse(List<Weather> weather, MainInfos main, Wind wind, Cloud clouds) {
        this.weather = weather;
        this.main = main;
        this.wind = wind;
        this.clouds = clouds;
    }
}
