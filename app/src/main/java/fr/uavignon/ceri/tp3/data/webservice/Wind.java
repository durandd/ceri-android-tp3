package fr.uavignon.ceri.tp3.data.webservice;

public class Wind {
    public final float speed;
    public final int deg;

    public Wind(float speed, int deg) {
        this.speed = speed;
        this.deg = deg;
    }
}
