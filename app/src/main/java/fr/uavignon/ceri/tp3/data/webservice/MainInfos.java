package fr.uavignon.ceri.tp3.data.webservice;

public class MainInfos {

    public final float temp;
    public final int humidity;

    public MainInfos(int temp, int humidity) {
        this.temp = temp;
        this.humidity = humidity;
    }
}
