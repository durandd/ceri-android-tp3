package fr.uavignon.ceri.tp3.data.webservice;

import fr.uavignon.ceri.tp3.data.City;

public class WeatherResult {

    public static void transferInfo(WeatherResponse weatherInfo, City cityInfo){
        cityInfo.setTempKelvin(weatherInfo.main.temp);
        cityInfo.setHumidity(weatherInfo.main.humidity);
        cityInfo.setWindSpeedMPerS(weatherInfo.wind.speed);
        cityInfo.setWindDirection(weatherInfo.wind.deg);
        cityInfo.setCloudiness(weatherInfo.clouds.all);
        cityInfo.setIcon(weatherInfo.weather.get(0).icon);
        cityInfo.setDescription(weatherInfo.weather.get(0).description);
        cityInfo.setLastUpdate(System.currentTimeMillis() / 1000L);
    }
}